package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> 
{

	@Override
	public int compare(Car HotWheel1, Car HotWheel2)
	{
		if(!(HotWheel1 instanceof Car) && !(HotWheel2 instanceof Car))
		{
			throw new IllegalStateException("Object inst an instance of car.");
		}
		
		if(HotWheel1.getCarBrand().compareTo(HotWheel2.getCarBrand())==0)
		{
			if(HotWheel1.getCarModel().compareTo(HotWheel2.getCarModel())==0)
			{
					return HotWheel1.getCarModelOption().compareTo(HotWheel2.getCarModelOption());
			}
			return HotWheel1.getCarModel().compareTo(HotWheel2.getCarModel());
		}
		return HotWheel1.getCarBrand().compareTo(HotWheel2.getCarBrand());
		//String Hwheel1 = HotWheel1.getCarBrand() + HotWheel1.getCarModel() + HotWheel1.getCarModelOption();
		//String Hwheel2 = HotWheel2.getCarBrand() + HotWheel2.getCarModel() + HotWheel2.getCarModelOption();
		
		//return Hwheel1.compareTo(Hwheel2);
	}
}

