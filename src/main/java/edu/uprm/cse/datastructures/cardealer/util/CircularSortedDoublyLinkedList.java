package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	private class Node<E>{
		private E element;
		private Node<E> prev;
		private Node<E> next;
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		
	}
	private int currentsize;
	private Node<E> header;
	private Comparator<E> comp;
	
	public CircularSortedDoublyLinkedList(Comparator<E> c) {
		header = new Node<E>();
		currentsize = 0;
		
		this.header.setElement(null);
		this.header.setPrev(header);
		this.header.setNext(header);
		this.comp = c;
	}
	
	

	//@Override
	//	public Iterator<E> iterator() 
	//	{
	//		List<E> iterlist = new ArrayList<E>();
	//		Node<E> pos = this.header.getNext();
	//		while(pos != this.header)
	//		{
	//			iterlist.add(pos.getElement());
	//			pos = pos.getNext();
	//		}
	//		return iterlist.iterator();
	//	}
	private class CircularSortedDoublyLinkedListIterator <E> implements Iterator<E>
	{
		private Node<E> nextNode;
		public CircularSortedDoublyLinkedListIterator() 
		{
			this.nextNode = (Node<E>) header.getNext();
		}
		 @Override
		 public boolean hasNext()
		 {
			 return nextNode != header;
		 }
		 @Override
		 public E next()
		 {
			 if(this.hasNext())
			 {
				 E result = this.nextNode.getElement();
				 this.nextNode = this.nextNode.getNext();
				 return result;
			 }
			 else 
			 {
				 throw new NoSuchElementException();
			 }
		 }
		
	}
	@Override
	public Iterator <E> iterator()
	{
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		Node<E> temp = new Node<E>();
		temp.setElement(obj);
		if(obj == null) {
			return false;
		}
		else {
			for(Node<E> position = this.header.getNext(); position != this.header; position = position.getNext()) {
				//the loop checks if obj is smaller than any element already on the list.
				if(comp.compare(obj, position.getElement()) <= 0) {
					temp.setNext(position);
					temp.setPrev(position.getPrev());
					position.getPrev().setNext(temp);
					position.setPrev(temp);
					this.currentsize++;
					
					return true;
				}
			}
			//if this code is reached, it means the obj is bigger than all elements already on the list, and thus, gets placed at the end.
			temp.setPrev(this.header.getPrev());
			temp.setNext(this.header);
			this.header.setPrev(temp);
			temp.getPrev().setNext(temp);
			this.currentsize++;
			return true;
			
		}
	}

	@Override
	public int size() {
		return currentsize;
	}

	@Override
	public boolean remove(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		else if(!this.contains(obj)) {
			return false;
		}
		else {
			Node<E> temp = this.header.getNext();
			while(temp != this.header) {
				if(temp.getElement().equals(obj)) {
					if(temp.getNext() == null) {
						temp.getPrev().setNext(temp.getNext());
						temp.setPrev(null);
						temp.setNext(null);
						temp.setElement(null);
						this.currentsize--;
						return true;
					}
					else {
						temp.getNext().setPrev(temp.getPrev());
						temp.getPrev().setNext(temp.getNext());
						this.currentsize--;
						return true;
					}
				}
				temp = temp.getNext();
			}
			return false;
		}
	}

	@Override
	public boolean remove(int index) {
		int pos = 0;
		Node<E> temp = null;
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else if(this.isEmpty()) {
			return false;
		}
		else {
			temp = this.header.getNext();
			while(pos < index) {
				temp = temp.getNext();
				pos++;
			}
			temp.getPrev().setNext(temp.getNext());
			this.currentsize--;
			temp.setElement(null);
			temp.setNext(null);

			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		int result = 0;
		while(this.remove(obj)) {
			result++;
		}
		return result;
	}

	@Override
	public E first() {
		return this.isEmpty() ? null : this.header.getNext().getElement();
	}

	@Override
	public E last() {
		
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else {
			int pos = 0;
			Node<E> temp = this.header.getNext();
			while(pos < index) {
				temp = temp.getNext();
				pos++;
				
			}
			return temp.getElement();
		}
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return currentsize == 0;
	}

	@Override
	public int firstIndex(E e) {

		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp.getElement() != null; 
				temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int result = -1;
		if(e == null) {
			throw new IllegalArgumentException();
		}
		else {
			Node<E> temp = this.header.getNext();
			int position = 0;
			while(temp != this.header) {
				if(temp.getElement().equals(e)) {
					result = position;
				}
				temp = temp.getNext();
				position++;
			}
			
			return result;
			
		}
	}

}
