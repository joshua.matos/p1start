package edu.uprm.cse.datastructures.cardealer.util;

import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CSDLinkedListTester<E> {
	
	
	private CSDLinkedList<String> A;
	private CSDLinkedList<String> B;
	private CSDLinkedList<String> C;
	
	@Before
	public void setUp() throws Exception {
		Comparator<String> c1 = new StringComparator();
		Comparator<String> c2 = new StringComparator();
		Comparator<String> c3 = new StringComparator();
		A = new CSDLinkedList(c1);
		B = new CSDLinkedList(c2);
		C = new CSDLinkedList(c3);
		
		A.add("Pence");
		A.add("Olette");
		A.add("Hayner");
		A.add("Roxas");
		A.add("Hayner");
		
		C.add("Troy");
	}
	

	@Test
	public void testSize() {
		assertTrue(A.size() == 4);
		assertTrue(B.size() == 0);
	}
	@Test
	public void testRemove() {
		
		assertFalse(B.remove("Boi"));
		assertFalse(C.isEmpty());
		C.remove(0);
		assertTrue(C.isEmpty());
		
		A.remove(3);
		assertFalse(A.contains("Roxas"));
		assertTrue(A.get(0) == "Hayner");
		assertTrue(A.get(1) == "Olette");
		assertTrue(A.get(2) == "Pence");
		
		
	}
	@Test
	public void testRemoveAll() {
		assertTrue(A.size() == 4);
		A.removeAll("Olette");
		assertTrue(A.size() == 3);
		assertTrue(A.get(0) == "Hayner");
		assertTrue(A.get(1) == "Pence");
		assertTrue(A.get(2) == "Roxas");
	
	}
	@Test
	public void testAdd() {
		assertTrue(A.firstIndex("Hayner") == 0);
		assertTrue(A.firstIndex("Roxas") == 3);
	}
	@Test
	public void testFirst() {
		assertTrue(A.first() == "Hayner");
		assertTrue(B.first() == null);
	}
	@Test
	public void testClear() {
		assertTrue(A.size() == 4);
		A.clear();
		assertTrue(A.isEmpty());
		
	}
	

}
