//package edu.uprm.cse.datastructures.cardealer.util;
//
//public class CarList {
//
//}
package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList {
//	private static CarList singleton = new CarList();
//	private static SortedList<Car> carList = null;

//private CarList() {

	public static CircularSortedDoublyLinkedList<Car> CarList = new CircularSortedDoublyLinkedList<Car>( new CarComparator());
	//carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return CarList;
		}
	public static void resetCars() {
		CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		}
}
//
//public static CarList getInstance() {
//	return singleton;
//}
//
//public SortedList<Car> getCarList() {
//	return this.carList;
//}
//
//public static void resetCars() {
//	carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
//}
//
//}