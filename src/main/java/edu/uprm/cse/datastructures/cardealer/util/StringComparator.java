package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

public class StringComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		if(o1.equals(null)) {
			return -1;
		}
		else if(o2.equals(null)) {
			return 1;
		}
		else {
			return o1.compareTo(o2);
		}
	}

}
