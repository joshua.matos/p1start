package edu.uprm.cse.datastructures.cardealer;



import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager 
{
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() 
	{
		SortedList<Car> list = CarList.getInstance();
		Car[] newList = new Car[list.size()];
		int count=0;
		for(Car c:list)
		{
			newList[count++]=c;
		}
		return newList;
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) throws NotFoundException 
	{
		SortedList<Car> list =CarList.getInstance();
		for(Car c:list)
		{
			if(c.getCarId()==id) 
			{
				return c;
			}
		}
		throw new WebApplicationException(404);
		
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar) {
		SortedList<Car> list = CarList.getInstance();
		for(int i = 0;i<list.size();i++)
		{
			if(newCar.getCarId()==list.get(i).getCarId())
			{
				return Response.status(Response.Status.CONFLICT).build();
			}
		}
		list.add(newCar);
		return Response.status(201).build();
	}
	
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Car updatedCar) 
	{
		SortedList<Car> list = CarList.getInstance();
		for(Car c: list) 
		{
			if(c.getCarId()==updatedCar.getCarId()) 
			{
				list.remove(c);
				list.add(updatedCar);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
	@Path("{id}/delete")
	public Response deletePerson(@PathParam("id") long id)
	{
		SortedList<Car> list = CarList.getInstance();
		for(Car c:list) 
		{
			if(c.getCarId()==id)
			{
				list.remove(c);
				return Response.status(Response.Status.OK).build();
			}
		}
		//throw new NotFoundException("Customer"+id+"not found");
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	

}
